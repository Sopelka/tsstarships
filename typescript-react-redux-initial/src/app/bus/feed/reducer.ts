// Types
import {
    StarshipActionsTypes,
    START_FETCHING_S,
    STOP_FETCHING_S,
    FILL_STARSHIPS,
    FETCH_STARSHIPS_ASYNC,
    FillStarships,
    FillStarshipsActionType,
    Starship,
} from './types';

import { AnyAction } from 'redux';

export type StarshipState = {
    starships: Starship[],
    isFetching: boolean,
}

export const initialState: StarshipState = {
    starships: [],
    isFetching: false,
};

export const feedReducer = (state = initialState, { type, payload }: AnyAction ): StarshipState => {
    switch (type) {
        case START_FETCHING_S:
                return {...state, isFetching: true};
                
        case STOP_FETCHING_S:
            return {...state, isFetching: false};

        case FILL_STARSHIPS:
            return {...state, starships: payload};

        default:
            return state;
    }
};
