// export const types = {
//     // Sync
//     START_FETCHING: 'START_FETCHING',
//     STOP_FETCHING: 'STOP_FETCHING',
//     FILL_STARSHIPS: 'FILL_STARSHIPS',
//     // Async
//     FETCH_STARSHIPS_ASYNC: 'FETCH_STARSHIPS_ASYNC',
// };

export type Starship = {
    name?: string,
    starship_class?: string,
    manufacturer?: string,
    crew?: string,
}

export type FillStarships = {
    results: Starship[],
}


export const START_FETCHING_S = 'START_FETCHING';
export type StartFetchingActionType = {
    type: typeof START_FETCHING_S,
    payload?: never,
};

export const STOP_FETCHING_S = 'STOP_FETCHING';
export type StopFetchingActionType = {
    type: typeof STOP_FETCHING_S;
    payload?: never,
};

export const FILL_STARSHIPS = 'FILL_STARSHIPS';
export type FillStarshipsActionType = {
    type: typeof FILL_STARSHIPS;
    payload: Starship[],
};

export const FETCH_STARSHIPS_ASYNC = 'FETCH_STARSHIPS_ASYNC';
export type FetchStarshipsAsyncActionType = {
    type: typeof FETCH_STARSHIPS_ASYNC;
    payload?: never,
};

export type StarshipActionsTypes = 
    | StartFetchingActionType
    | StopFetchingActionType
    | FillStarshipsActionType
    | FetchStarshipsAsyncActionType;