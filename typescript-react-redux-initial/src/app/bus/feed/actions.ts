// Types
import {
    StarshipActionsTypes,
    START_FETCHING_S,
    STOP_FETCHING_S,
    FILL_STARSHIPS,
    FETCH_STARSHIPS_ASYNC,
    FillStarships,
    FillStarshipsActionType,
    Starship,
} from './types';

// Sync
export function StarshipStartFetching() : StarshipActionsTypes {
    return {
        type: START_FETCHING_S,
    };
};

export function StarshipStopFetching() : StarshipActionsTypes {
    return {
        type: STOP_FETCHING_S,
    };
};

export function fillStarships(starships : Starship[]) : FillStarshipsActionType {
    return {
        type: FILL_STARSHIPS,
        payload: starships,
    };
};

// Async
export function fetchStarshipsAsync() : StarshipActionsTypes {
    return {
        type: FETCH_STARSHIPS_ASYNC,
    };
};


// export const feedActions = {
//     // Sync
//     startFetching: () => {
//         return {
//             type: START_FETCHING,
//         };
//     },
//     stopFetching: () => {
//         return {
//             type: STOP_FETCHING,
//         };
//     },
//     fillStarships: (starships) => {
//         return {
//             type: FILL_STARSHIPS,
//             payload: starships,
//         };
//     },
//     // Async
//     fetchStarshipsAsync: () => {
//         return {
//             type: FETCH_STARSHIPS_ASYNC,
//         };
//     },
// };