// Types
import {
    ProfileActionsTypes,
    FILL_PROFILE,
    STOP_FETCHING_P,
    START_FETCHING_P,
    FillProfileActionType,
    ProfilePayloadType,
} from './types';

export function ProfileStartFetching() : ProfileActionsTypes {
    return {
        type: START_FETCHING_P,
    };
};

export function ProfileStopFetching() : ProfileActionsTypes {
    return {
        type: STOP_FETCHING_P,
    };
};

export function fillProfile(profile : ProfilePayloadType) : FillProfileActionType {
    return {
        type: FILL_PROFILE,
        payload: profile
    };
};




// export const profileActions = {
//     // Sync
//     fillProfile: (profile) => {
//         return {
//             type: types.FILL_PROFILE,
//             payload: profile
//         };
//     },
//     startFetching: () => {
//         return {
//             type: types.START_FETCHING,
//         };
//     },
//     stopFetching: () => {
//         return {
//             type: types.STOP_FETCHING,
//         };
//     },
// };
