// Types
import {
    ProfileActionsTypes,
    FILL_PROFILE,
    STOP_FETCHING_P,
    START_FETCHING_P,
    FillProfileActionType,
    ProfilePayloadType,
} from './types';


export type ProfileState = {
    firstName: string,
    lastName: string,
    isFetching: boolean,
}

const initialState: ProfileState = {
    firstName: 'Уолтер',
    lastName: 'Уайт',
    isFetching: false,
};


export const profileReducer = (state = initialState, { type, payload } : ProfileActionsTypes) : ProfileState => {
    switch (type) {
        case FILL_PROFILE:
            return {...state, ...payload};
        
        case START_FETCHING_P:
            return {...state, isFetching: true};
                
        case STOP_FETCHING_P:
            return {...state, isFetching: false};

        default:
            return state;
    }
};
