// export const types = {
//     // Sync
//     FILL_PROFILE: 'FILL_PROFILE',
//     START_FETCHING: 'START_FETCHING',
//     STOP_FETCHING: 'STOP_FETCHING',
// };

export type ProfilePayloadType = {
    firstName: string,
    lastName: string, 
    isFetching: boolean,
}

export const FILL_PROFILE = 'FILL_PROFILE';
export type FillProfileActionType = {
    type: typeof FILL_PROFILE;
    payload: ProfilePayloadType;
};

export const STOP_FETCHING_P = 'STOP_FETCHING';
export type StopFetchingActionType = {
    type: typeof STOP_FETCHING_P;
    payload?: never;
};

export const START_FETCHING_P = 'START_FETCHING';
export type StartFetchingActionType = {
    type: typeof START_FETCHING_P;
    payload?: never;
};

export type ProfileActionsTypes = 
    | FillProfileActionType
    | StopFetchingActionType
    | StartFetchingActionType;