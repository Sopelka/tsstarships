// Core
import React, { FC } from 'react';
import { useSelector, useDispatch } from 'react-redux';

// Instruments
import Styles from './styles.module.css';
import { fetchStarshipsAsync } from '../../bus/feed/actions';
import { StarshipTile } from '../StarshipTile';

import { AppState } from '../../init/rootReducer';
import { StarshipState } from '../../bus/feed/reducer';

export const Panel: FC = () => {
    const dispatch = useDispatch();
    const starships = useSelector<AppState, StarshipState["starships"]>((state) => state.feed.starships);
    const isFetching = useSelector<AppState, StarshipState["isFetching"]>((state) => state.feed.isFetching);

    const _fetchPostsAsync = () => {
        return dispatch(fetchStarshipsAsync());
    };

    const starshipsJSX = starships.map((starship) => {
        return (
            <StarshipTile
                key = { starship.name }
                {...starship}
            />
        )
    });

    const buttonMessage = isFetching
        ? '⏳ Вызываю...'
        : '📲 Вызвать корабли';

    return (
        <section className = { Styles.panel }>
            <h1>🖥</h1>
            <button
                disabled = { isFetching }
                onClick = { _fetchPostsAsync }
            >
                {buttonMessage}
            </button>
            <ul>{ starshipsJSX }</ul>
        </section>
    );
}

// Before
// // Core
// import React from 'react';
// import { useSelector, useDispatch } from 'react-redux';

// // Instruments
// import Styles from './styles.module.css';
// import { feedActions } from '../../bus/feed/actions';
// import { StarshipTile } from '../StarshipTile';

// export const Panel = () => {
//     const dispatch = useDispatch();
//     const starships = useSelector((state) => state.feed.starships);
//     const isFetching = useSelector((state) => state.feed.isFetching);

//     const _fetchPostsAsync = () => {
//         return dispatch(feedActions.fetchStarshipsAsync());
//     };

//     const starshipsJSX = starships.map((starship) => {
//         return (
//             <StarshipTile
//                 key = { starship.name }
//                 {...starship}
//             />
//         )
//     });

//     const buttonMessage = isFetching
//         ? '⏳ Вызываю...'
//         : '📲 Вызвать корабли';

//     return (
//         <section className = { Styles.panel }>
//             <h1>🖥</h1>
//             <button
//                 disabled = { isFetching }
//                 onClick = { _fetchPostsAsync }
//             >
//                 {buttonMessage}
//             </button>
//             <ul>{ starshipsJSX }</ul>
//         </section>
//     );
// }