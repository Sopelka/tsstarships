// Core
import React, { FC } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Formik, Form, Field } from 'formik';
import cx from 'classnames';

// Instruments
import Styles from './styles.module.css';
import { ProfileStartFetching, ProfileStopFetching, fillProfile } from '../../bus/profile/actions';

import { AppState } from '../../init/rootReducer';
import { ProfileState } from '../../bus/profile/reducer';

export const Profile: FC = () => {
    const dispatch = useDispatch();

    const _submit = (values, actions) => {
        dispatch(ProfileStartFetching());
        actions.setSubmitting(false);
        dispatch(fillProfile(values));
        dispatch(ProfileStopFetching());
    };

    const profile = useSelector<AppState, ProfileState>((state) => state.profile);
    const { firstName, lastName, isFetching } = profile;

    const buttonMessage = isFetching ? 'Обновляю' : 'Обновлено';

    const disabledStyle = cx({
        [ Styles.disabled ]: isFetching,
    });

    return (
        <section className = { Styles.profile }>
            <h1>
                {firstName} {lastName}
            </h1>
            <Formik
                initialValues = {{ firstName, lastName }}
                render = { () => (
                    <Form>
                        <Field
                            className = { disabledStyle }
                            disabled = { isFetching }
                            name = 'firstName'
                            type = 'text'
                        />
                        <Field
                            className = { disabledStyle }
                            disabled = { isFetching }
                            name = 'lastName'
                            type = 'text'
                        />
                        <button
                            className = { disabledStyle }
                            disabled = { isFetching }
                            type = 'submit'>
                            {buttonMessage}
                        </button>
                    </Form>
                ) }
                onSubmit = { _submit }
            />
        </section>
    );
}
